float Y = 5.0;
float alpha = 0.05;
float S = Y;

void setup() {
  // put your setup code here, to run once:
  pinMode(A1,INPUT);
  Serial.begin(9600);
}
void loop() {
  int val = analogRead(A1);
  float volt = float(val)/1023*5.0;
  float ph=21.19-5.6*volt;
  S=(alpha*ph)+((1-alpha)*S);

  Serial.print("pH= ");
  Serial.println(ph);
  Serial.print("pH corr= ");
  Serial.println(S);
delay(100);
}
